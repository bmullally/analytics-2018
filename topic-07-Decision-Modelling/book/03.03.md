# One Way Data Tables

We are now going to cover how to use a data table to gather multiple outcomes from a formula given a variable input. After this demonstration, you should be able to use a one way data table to understand the range of outcomes. For a given range of a variable input. 

We're going to analyze the impact of flight related costs. The forecasting team is looking to get a better understanding of how fuel and labor affect profitability in a proposed new route from Chicago to Atlanta. 

The operations team has determined the minimum cost per mile is $30, and the maximum cost per mile is $50, depending on the various market factors. The operations team has also provided estimates for the number of passengers per flight and the revenue per passenger. 170 and 150 respectively. 

We've summarized this information in cells B113 through C120. Using the information provided by the operations team, we've determined an estimated profit per flight in cell C120, however, recall that we would like to analyze how profit is impacted by cost per flight. This table only calculates the profits for one value of cost per mile at a time. 

![](./img/21.png)

However in reality, we are rarely confident on an exact number for the input for one of our variables. This is where the data table tool provides value, as it's going to allow us to fluctuate the value of the cost per mile variable. We've provided the structure of the data table and cells, F113 through G124. 

![](./img/22.png)


Now we'll demonstrate how to use a one way data table to analyze the impact of cost per mile on profit. 

First, we need to link our objective formula located in cell C120 to the data table. 
 - Select G113 and type the equal sign. 
 - Then select C120 
 
 ~~~
 =C120
 ~~~

 - Next, notice the range of values in cells F114 through F124. 
 - Each cell in this range represents a particular value of our variable, cost per mile, which is located in cell C117. 
 - When the data table is complete, for each value in cost per mile range, we'll have an outcome for a formula cell C120. 
 - select cells F113, through G124. 
 - Navigate to the data tab in the ribbon, and selected data table from the what if analysis drop down.
 - We now are presented with two options, row input cell and column input cell. 
 - In this case, we have a column-oriented one way data table, because our range of values is located in a column. 
 - Our variable input is cost per mile, so using the reference box select cell C117 and click okay. 
 - Each value in cells G114 to G124 represents the outcome of the formula in cells C120, for each respective value in cells F114 to G124. 



Notice that when cost per mile is $50, profit falls to $0. We've now demonstrated how to use a one way data table, to determine the range of outcomes given a variable input. In this case, cost per mile. 

![](./img/23.png)
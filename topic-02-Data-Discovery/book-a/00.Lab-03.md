Objectives

This lab will focus on how to effectively summarise data quantitatively and perform some basic analyses for useful managerial information and insight. You will also cover the use of histograms, box plots and scatter plots.
#Exercises

A community health status survey obtained the following demographic information from the respondents:

![](./img/15.png)

Compute the relative frequency and cumulatvie relative frequency of the age groups.

![](./img/16.png)

The Excel file [MBA Student Survey](./archives/MBAStudentSurvey.xlsx) provides demographic data and and responses to questions on the number of nights out per week and study hours per week for a group of MBA students. Construct frequency distributions and compute the relative frequencies for the categorical variables of gender, international students status, and undergraduate concentration. What conclusions can you draw?

Construct a fequency distribution and histogram for driving acuracy(%) in the Excel file [GolfingStatistics](./archives/GolfingStatistics.xlsx) using the Excel Histogram tool and appropriate bin ranges. find the relative frequencies and cumulative relative frequencies for each bin.

When the Histogram is created in the sepearate sheet, add a relative frequency and cumulative frequency column as stated above. Add another series by right clicking on the histogram, and choosing Select Data. Click Add, name the series cumulative, choose the cumulative column of data you constructed. Next change the series from a bar to a line on the chart. Do this by right clicking on the cumulative series and choose Change Series Chart Type, choose Line. Next right click the series and choose Format Data Series, Change the axes to a secondary Axes.

Your chart and data should look like this:

![](./img/17.png)

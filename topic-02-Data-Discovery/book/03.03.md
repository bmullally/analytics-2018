#Data Profiles

Data are often expressed as percentiles and quartiles. You are no doubt familiar with percentiles from standardised tests such as those used for college entrance exams. Percentiles specify the percentage of other test takers who scored at or below the score of a particular individual.

Generally the kth percentile is a value at or below which at least k percent of the observations lie. There is not a standardised method for calculating the kth percentile. However the most common is as follows:

First order the N data values from smallest to largest and calculate the rank of the kth percentile using the following formula:

~~~
Nk/100 + 0.5

rounded to the nearest integer
~~~

Then you take the value corresponding to that rank as the kth percentile.

For example, using the Facebook data, the rank of the 90th percentile would be computed as:

~~~
33(90)/100 + 0.5 = 30.2

rounded to nearest integer is 30
~~~

The 30th rank is 400, so 90 percent of students have 400 or less friends.

![](./img/14.png)

Excel does have another method of calculating this using the PERCENTILE function but the values calculated can vary from the usual method.

~~~

percentile.inc(e4:e36,0.9) = 396

this gives the 90th percentile as 396 or rounded to 400.
~~~

Quartiles represent the 25th percentile (called the first quartile, Q1), 50th percentile (second quartile, Q2), 75th percentile (third quartile, Q3), and the 100th percentile (fourth quartile, Q4). One-forth of the data is below the first quartile, and two-forths of the data are below the second quartile.

For example in the facebook data,

quartile.inc(e4:e36,1) = 80 = Q1
This means 25% of the data falls below 80 Friends.

quartile.inc(e4:e36,3) = 250 = Q3
This means 75% of the data falls below 250 Friends.


We can extend these ideas to other divisions of the data. For example, deciles divide the data into 10 sets: the 10th percentile, 20th percentile, and so on. If you want the 10th percentile then you can use the percentile.inc formula with 0.1 in the formula.

All these types of measures are called data profiles or fractiles.

(the same information can be seen in your cumulative relative frequency column)

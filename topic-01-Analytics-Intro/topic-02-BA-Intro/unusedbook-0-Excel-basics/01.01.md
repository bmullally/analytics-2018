#Using Microsoft Excel

Spreadsheet software for personal computers has become an indispensable tool for business analysis, particularly for the manipulation of numerical data and the development and analysis of decision models. Some differences exist between 2007 and 2010. If you do use another version you should be able to apply Excel easily to the problems and exercises. In addition it must be noted that Mac versions of Excel do not have the full functionality that Windows versions have.

Although Excel has some flaws and limitations from a statistical perspective, its widespread availability makes it the software of choice for many business professionals.


##Instructions for working in Walton Building PC Labs:

If you are working on the workstations in the IT Building all machines should be installed with Microsoft Excel 2010.

Proceed with the next step of the lab.

##Instructions for working on your own laptop
You must have a copy of Microsoft Excel 2010  or newer installed.


There will be adons utilised in future practicals but for the moment the standard installation of Excel is sufficient.
The student site that supports a book used in this module contains the links to software, along with data files and other materials.


- <http://wps.pearsoned.co.uk/ema_ge_evans_statdata_5e/227/58333/14933382.cw/index.html>

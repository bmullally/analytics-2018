#Functions on a set of numbers

The natural log of a number is used in business to figure out growth rates over time.
ln(x) is the natural log of number x. 
The natural log of 1.5:

- Assuming 100% growth rate you are essentially asking how long does it take to grow to get to 1.5, the answer is .405, less than half the time period) 
- Assuming 1 period of time, how much do you need to grow to get to 1.5 (40.5% per year)

You are either looking at growth rate or time period.

Save the following stock prices file.

- [Stock Prices](./archives/stockPrices.xlsx)

We can perform functions on more than one variable such as pairs, two rows or two columns perhaps. This data is from Yahoo finance for the Standard and Poor's 500 index (US stock market index) and Duke Energy stock prices. The price is the closing price from the begining of each month. 176 months of price data. We want to caculate the continuously commounded monthly return. To do this we take the natural log of the ratio between the two prices with the more recent price on top. 

We do the natural log function of the price in August, divided by the price in July, this gives us a loss of -.29% In August the index was 1925.15 and in July it was 1930.67

This applies the natural log function to show the monthly continuously compounded return.

![](./img/02.png)


Now double click on the handle of cell D4 so that the remainder of the cell in column D are populated. Then copy the formula to cell E4 and populate all column E.

Duke Energy prices show as slightly up for the month 1.12%.

This shows two time series of monthly returns for the index and for an idividual stock. You may now want to see how one stock performed agains the larger market it is a part of. Which performed better?

To calcualte the average of a series of values that are all in one column or one row (an array), we write a formula into cell G6 

~~~

=Average(D4:D178).

~~~

Then copy that relative reference formula over to cell H6

To find the average annual return in cell G8 then we multiply by 12. Again copy this formula over to H8.

Calculate the standard deviation of monthly returns in cell G10 using the stdev.p() function.

To calcuate the annualised standard deviation we do not simply multiply by 12 we must multiple by the squareroot of 12. Excel has a function for this so in cell G12 enter the following:


~~~

=G10*sqrt(12)

~~~

Now calculate the minimum and maximum for both the index and for Duke Energy stock.

We can see from the descriptive statistics that conclusions can be made; Duke Energy Stock has been much higher paying investment that investing in S&P over this time ineterval (14 years), there was a higher standard deviation of returns so Duke is not a free deal it has some cost in terms of risks.
#Using a Column Chart

Open the Exercise1 tab.

Let's look at how to use column charts. We want to look at the routes that have had the biggest change in passenger capacity in the last year.

The first two columns of the table contain the FlightID and a RouteID which identify the flight and route flown by that flight. The following two columns provide the Passenger Volumes for both 2014 and 2015. We're going to use the last column that is currently empty to calculate the percentage change in passenger volume between 2014 and 2015.

The first question in this exercise asks us to calculate the change percentage in column G. The formula to calculate this value is going to be passenger volume in 2015 minus the passenger volume in 2014 divided by the total passenger volume for 2014.

~~~
=(F15-E15)/E15)
~~~


We can now see that for flight number 9, the passenger volume increased by 20% between 2014 and 2015. Now that we have validated that our formula works correctly, let's go ahead and copy it down the rest of the table. The order of the data in this table will be replicated in a graph. Therefore we're going to want to sort the data before creating the graph. To do this, we're going to select cell G14. And navigate to the editing section on the home tab. Next, we click on the sort and filter button. And select sort a through z from the menu. As you can see, the Chicago to Seattle route has experienced the largest decrease in passenger volumes.

Remember the charting principles that we discussed previously. Less data helps you send a clearer message to the audience. Our focus here is on determining the routes for which the passenger volumes have changed the most. Hence, we don't need to include the data that is in column C, E, or F in our chart. So let's go ahead and select the two columns that we're interested in by holding down the Ctrl key, and selecting the values in column D and G. Once the data has been selected, we're going to click on the Insert tab on the ribbon, and select the Recommended Charts option. From here, we select the clustered column chart, which creates our graph.

![](./img/04.png)

We’re going to spend some time formatting this chart so that it conveys the message that we're trying to share with our audience. First, let’s move the chart so that it is no longer on top of the data table. We do this by clicking on the chart and dragging it into the desired position.

Next, we are going to change the title of our chart to something that is more descriptive of what is being shown. Let's click on the title, select the placeholder text, and replace it with "% change in passenger volume for each route ID".

We also want to make sure that we adjust the positioning of the axis labels, so that they become legible to the audience. To do this, we're going to right click on the horizontal axis. And select the Format Axis option from the menu. In the Format Axis panel, we're going to scroll down and expand the Labels category.

Next, we click on the Label Position drop-down and select Low. As you can see, the axis labels are now below the negative outcomes making the route IDs easier to read. We are also going to add titles to the horizontal and vertical axes. This is done by clicking on a chart, going to design tab, and selecting the add chart element option. We click on access titles and then we select a primary horizontal. In the primary vertical options.

To edit the axis titles we just created, we're going to click on the box for the horizontal axis title, and replace the text with route ID. Same thing for the vertical axis.

Let's click on the title. And replace it with, % change from 2014 to 2015. It would also be helpful to change the colors of the bars to helps us distinguish between the positive and the negative changes in passenger volumes. To do this we're going to click one of the bars in order to select the data series. Now that we have selected the bars representing the negative changes, we're going to bring up the format data point panel by right clicking on our selection.

We're going to click on the fill option and select a shade of red from the drop down menu to represent the fact that these are decreases in passenger volumes. We now have a column chart that shows us the changes in passenger volumes between 2014 and 2015 across all routes. We can see that despite a couple of routes losing passengers, overall, most routes increased their passenger volumes between 2014 and 2015.

![](./img/05.png)

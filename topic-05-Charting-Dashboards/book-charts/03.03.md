#Combo chart

In Exercise two, we are going to look at combo charts in more detail. We're being asked to analyze the passenger volumes for the Chicago to Seattle route in order to determine whether a new competitor who entered the market in March of 2015 has affected our sales. Based on the results of our analysis, the marketing department will be able to decide whether or not it's necessary to launch an expensive marketing campaign to retain our customers.

We have been given a dataset with the monthly passenger volumes on the Chicago to Seattle route for both 2014 and 2015. Using this data, we're going to determine whether the entry of a new competitor into the market in March 2015 has effected the overall passenger volumes. To fully understand how this route has been effected by the change of the market, we're going to look at the difference in passenger volumes between 2014 and 2015 on a monthly basis as well as their cumulative differences, since this will help us understand the net effect of the changes as well as reducing some of the noise from the monthly data points.


![](./img/06.png)


Let's start this exercise by completing the fields in column K and L in the data table. We will start with the difference from 2014 in column K.
In cell K17 type:

~~~
= D17-E17
~~~

Copy the formula down to the last row.  We can already see that there was definitely a change in the overall trends somewhere in April. The passenger volumes were slightly higher in 2015 than in 2014 for January, February and March. However, in April, this train reversed and the passenger volumes were a lot lower than the previous year for the remaining months. It definitely looks like the new competitor affected our passenger volumes. So let's finish with our cumulative difference, so that we can start plotting these values.

In cell L17 type:
~~~
=K17
~~~

The cumulative difference for the current month is going to be equal
to the cumulative difference from the previous month plus the volume
difference for the current month.
In cell L18 type:
~~~
=L17+K18
~~~

This formula applies for the remaining months of the year, so let's copy it down the table. As we can see, the cumulative difference in passenger volumes went up in the beginning of the year, but started decreasing in April and went negative the following month.

![](./img/07.png)

Now that we have our data and already see some worrying trends, let's focus on how we're going to present this analysis to our audience. We want to create a chart that allows us to show both monthly differences in passenger volumes, as well as the cumulative difference over the year.

This type of data is best shown using a Combo Chart. We're going to start by selecting the two sets of data that we want to plot, which is going to be the data in columns K and L that we just filled out. Note that we're not including the route information since this is constant across our data points and wouldn't be adding anything to the chart. So let's go ahead and select the two columns with the monthly difference and cumulative difference in passenger volume, making sure we select the headers of our dataset.

![](./img/08.png)

Once the data is selected, let's navigate to the Insert tab on the Excel ribbon where we're going to click on the Combo Chart button and then click on the create custom Combo Chart option. We want the monthly differences to be plotted as a clustered column chart whereas we want the cumulative
differences to be plotted as a line chart to show the trends over the year.

The data points for the cumulative differences go from positive 5,000 to negative 24,000, but the data points for the monthly differences only go from
positive 800 to negative 4,000. So, it doesn't make sense to plot them on the same axis. Hence, we're going to select the check box for the secondary axis for the cumulative differences.

![](./img/09.png)


Now that we have provided all the required input, let's select OK to create our Combo Chart. We can see on this chart that something happened in the March to April time frame that completely changed the trend for the passenger volumes. Now the question is how can we make it easier for our audience to draw the same conclusion from this data?

As discussed this week in lectures, we can improve our chart by formatting it to emphasis the story that we are trying to tell. Let's start by changing the title of the chart. To do this, we're going to select the placeholder text and replace it with O'Hare, O-R-D to Seattle, SEA Passenger Variance from 2014 to 2015.

Next, we're going to adjust the positioning of the axis labels. Let's right-click on the Y-axis and select Format Axis from the menu. In the pop-up window, expand the labels category. Click on the label position drop-down and select low. Once we close this pop-up, we can see the adjustment. We also want to make sure our axis have titles to make the chart easier to read. Since there are currently no axis titles, we're going to need to add
a chart element to our chart. So, let's click on our chart and navigate to the design tab on the ribbon where we click on the Add Chart Element button.


We want to add axis titles, so let's select that from the menu, then we are going to click on the Primary Horizontal and Primary Vertical options. Note that this chart has two vertical axis, so let's make sure we also add the secondary vertical axis. Next, we are going to add each of the axis titles. Lets start by selecting the place order text for the horizontal axis and replacing it with month. Next, we'll do the title for the axis on the left side of the graph. So, let's go ahead and select the placeholder text and replace it with 2014 to 2015 variants. Finally, let's select the placeholder text for the final vertical axis title and replace it with 2014 to 2015 cumulative variants.


We're going to add data labels by clicking on the chart and navigating to the design tab again. Let's click on the Add Chart Element button. Select Data Labels and click on More Data Options. As you can see, there are multiple layer options available to us. We're going to select the Outside End
as the label position and then we close the pop-up window. Since the chart is kind of small, the labels are a bit hard to read. So, lets make the chart slightly larger by clicking on the border of the chart and using the corner to re size it. Generally, grid lines are not necessary
when data labels are displayed. So let's go ahead and remove them from the chart since it will provide a cleaner look, and view. We're going to do this by clicking on the graph and selecting the plus button on the top right side. This brings up the chart elements menu where we are going to unselect the grid lines option.

![](./img/10.png)


Our graph answers the question asked by the CEO. The entry of a competitor into the market in March of 2015 clearly impacted our passenger volumes negatively. This chart can now be used by the marketing department to build a business case for creating an aggressive marketing campaign to retain our customers. After creating a graph, always revisit the message that you're trying to convey. This graph shows the passenger difference throughout the year in a bar graph, and a cumulative difference in a line graph.

Does the graph show a correlation with the competitor's entry into the market? It does. Remember though, take the findings with a grain of salt. Though there may be a correlation, we cannot say for certain that the competitor's entry is the cause for the passenger variance. 
